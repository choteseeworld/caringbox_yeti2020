//Hardware Connections (Breakoutboard to Arduino):
//  -5V = 5V (3.3V is allowed)
//  -GND = GND
//  -SDA = A4 (or SDA)
//  -SCL = A5 (or SCL)
//  -INT = Not connected

#include <Wire.h>
#include "MAX30105.h"
#include "spo2_algorithm.h"
#include "heartRate.h"
#include <WiFi.h>
#include <IOXhop_FirebaseESP32.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// Config Firebase
#define FIREBASE_HOST "yeti2020-4e2ff.firebaseio.com"
#define FIREBASE_AUTH "Ests30zqkC5kTEPSALfjQkGyGtd92M1wdKspxYDL"

// Config connect WiFi
#define WIFI_SSID "XEUS.DEV-2.4GHZ"
#define WIFI_PASSWORD "embeddednu2020"

// Define NTP Client to get time
WiFiClient espClient;
WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "time2.navy.mi.th", 3600, 60000);

String deviceName = "Jukkrid";
float temperature;
int pres_up = 120;
int pres_down = 80;

MAX30105 particleSensor;

#define MAX_BRIGHTNESS 255

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
//Arduino Uno doesn't have enough SRAM to store 100 samples of IR led data and red led data in 32-bit format
//To solve this problem, 16-bit MSB of the sampled data will be truncated. Samples become 16-bit data.
uint16_t irBuffer[100]; //infrared LED sensor data
uint16_t redBuffer[100];  //red LED sensor data
#else
uint32_t irBuffer[100]; //infrared LED sensor data
uint32_t redBuffer[100];  //red LED sensor data
#endif

int32_t bufferLength; //data length
int32_t spo2; //SPO2 value
int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
int32_t heartRate; //heart rate value
int8_t validHeartRate; //indicator to show if the heart rate calculation is valid

byte pulseLED = 11; //Must be on PWM pin
byte readLED = 1; //Blinks with each data read

unsigned long interval = 200;
unsigned long interval_1 = 200;
unsigned long interval_2 = 200;

unsigned long previousMillis = 0;
unsigned long previousMillis_1 = 0;
unsigned long previousMillis_2 = 0;



bool buzzerState = false;

const int buzzerPin = 16;
const int buttonPin = 17;

void setup()
{
  Serial.begin(115200); // initialize serial communication at 115200 bits per second:

  WiFi.mode(WIFI_STA);
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("..");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  timeClient.begin();
  pinMode(pulseLED, OUTPUT);
  pinMode(readLED, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(buttonPin, INPUT);

  // Initialize sensor
  if (!particleSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Serial.println(F("MAX30105 was not found. Please check wiring/power."));
    while (1);
  }

  digitalWrite(buzzerPin, !buzzerState);

  Serial.println(F("Attach sensor to finger with rubber band. Press button to start conversion"));
  while (digitalRead(buttonPin) == 0) ; //wait until user presses a key
  Serial.read();

  byte ledBrightness = 60; //Options: 0=Off to 255=50mA
  byte sampleAverage = 4; //Options: 1, 2, 4, 8, 16, 32
  byte ledMode = 2; //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  byte sampleRate = 100; //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  int pulseWidth = 411; //Options: 69, 118, 215, 411
  int adcRange = 4096; //Options: 2048, 4096, 8192, 16384

  particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange); //Configure sensor with these settings

}

void loop()
{
  timeClient.update();
  unsigned long currentMillis = millis();
  //unsigned long currentMillis_1 = millis();
  //unsigned long currentMillis_2 = millis();

  bufferLength = 100; //buffer length of 100 stores 4 seconds of samples running at 25sps

  //read the first 100 samples, and determine the signal range
  for (byte i = 0 ; i < bufferLength ; i++)
  {
    while (particleSensor.available() == false) //do we have new data?
      particleSensor.check(); //Check the sensor for new data

    redBuffer[i] = particleSensor.getRed();
    irBuffer[i] = particleSensor.getIR();
    particleSensor.nextSample(); //We're finished with this sample so move to next sample

    temperature = particleSensor.readTemperature();

    Serial.print(F("red="));
    Serial.print(redBuffer[i], DEC);
    Serial.print(F(", ir="));
    Serial.println(irBuffer[i], DEC);

    if ((unsigned long)(currentMillis - previousMillis) >= interval) {
      buzzerState = !buzzerState; // "toggles" the state
      digitalWrite(buzzerPin, buzzerState); // sets the LED based on ledState
      // save the "current" time
      previousMillis = millis();
    }

  }
  digitalWrite(buzzerPin, !buzzerState);

  //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
  maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
  //Serial.println("hr_function");
  //Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
  while (1)
  {
    //    unsigned long currentMillis = millis();
    unsigned long currentMillis_1 = millis();
    unsigned long currentMillis_2 = millis();
    //dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
    for (byte i = 25; i < 100; i++)
    {
      redBuffer[i - 25] = redBuffer[i];
      irBuffer[i - 25] = irBuffer[i];
    }

    //take 25 sets of samples before calculating the heart rate.
    for (byte i = 75; i < 100; i++)
    {
      while (particleSensor.available() == false) //do we have new data?
        particleSensor.check(); //Check the sensor for new data

      digitalWrite(readLED, !digitalRead(readLED)); //Blink onboard LED with every data read

      redBuffer[i] = particleSensor.getRed();
      irBuffer[i] = particleSensor.getIR();
      particleSensor.nextSample(); //We're finished with this sample so move to next sample

    }

    if (heartRate >= 100) {
      //Serial.print("STATE_1");
      //Serial.print("\t");
      heartRate = heartRate / 2;

      if (spo2 == -999 || spo2 < 82) {
        //digitalWrite(buzzerPin, buzzerState);
        if ((unsigned long)(currentMillis - previousMillis_1) >= interval_1) {
          buzzerState = !buzzerState; // "toggles" the state
          digitalWrite(buzzerPin, buzzerState); // sets the LED based on ledState
          // save the "current" time
          previousMillis_1 = millis();
          Serial.println("Calculate again!!!");
        }
        maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
      }
      else {
        Serial.print(F("HR="));
        Serial.print(heartRate, DEC);

        Serial.print(F(", HRvalid="));
        Serial.print(validHeartRate, DEC);

        Serial.print(F(", SPO2="));
        Serial.print(spo2, DEC);

        Serial.print(F(", SPO2Valid="));
        Serial.print(validSPO2, DEC);

        Serial.print(", temperatureC=");
        Serial.println(temperature, 4);

        StaticJsonBuffer<200> jsonBuffer;
        JsonObject& valueObject = jsonBuffer.createObject();
        valueObject["timestamp"] = timeClient.getEpochTime() - 3600;
        valueObject["Heart_Rate"] = heartRate;
        valueObject["Temperature"] = temperature;
        valueObject["Oxygen"] = spo2;

        JsonObject& Pressure = valueObject.createNestedObject("Pressure");
        Pressure["Pressure_up"] = pres_up;
        Pressure["Pressure_down"] = pres_down;

        Firebase.push(deviceName + "/value", valueObject);
        Serial.println("Finished send data to server!!!");
        delay(2000);
        ESP.restart();

      }

    }
    else {
      //Serial.print("STATE_2");
      //Serial.print("\t");
      if (spo2 == -999 || spo2 < 82) {
        //digitalWrite(buzzerPin, buzzerState);
        if ((unsigned long)(currentMillis - previousMillis_2) >= interval_2) {
          buzzerState = !buzzerState; // "toggles" the state
          digitalWrite(buzzerPin, buzzerState); // sets the LED based on ledState
          // save the "current" time
          previousMillis_2 = millis();
          Serial.println("Calculate again!!!");
        }
        maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
      }
      else {
        Serial.print(F("HR="));
        Serial.print(heartRate, DEC);

        Serial.print(F(", HRvalid="));
        Serial.print(validHeartRate, DEC);

        Serial.print(F(", SPO2="));
        Serial.print(spo2, DEC);

        Serial.print(F(", SPO2Valid="));
        Serial.print(validSPO2, DEC);

        Serial.print(", temperatureC=");
        Serial.println(temperature, 4);

        StaticJsonBuffer<200> jsonBuffer;
        JsonObject& valueObject = jsonBuffer.createObject();
        valueObject["timestamp"] = timeClient.getEpochTime() - 3600;
        valueObject["Heart_Rate"] = heartRate;
        valueObject["Temperature"] = temperature;
        valueObject["Oxygen"] = spo2;

        JsonObject& Pressure = valueObject.createNestedObject("Pressure");
        Pressure["Pressure_up"] = pres_up;
        Pressure["Pressure_down"] = pres_down;

        Firebase.push(deviceName + "/value", valueObject);
        Serial.println("Finished send data to server!!!");
        delay(2000);
        ESP.restart();

      }
    }

  }
}
